const http = require('http');
let port = 4000;

http.createServer(function(request, response){

	//GET method
	if(request.url ==="/" && request.method === "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Welcome to booking system")
	}
	//GET method
	else if(request.url ==="/profile" && request.method === "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Welcome to your profile!")
	}
	//GET method
	else if(request.url ==="/courses" && request.method === "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Here's our courses available")
	}
	//POST method
	else if(request.url ==="/addcourse" && request.method === "POST"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Add a course to our resources")
	}
	//PUT method
	else if(request.url ==="/updatecourse" && request.method === "PUT"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Update a course to our resources")
	}
	//DELETE method
	else if(request.url ==="/archivecourses" && request.method === "DELETE"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Archive courses to our resources")
	}
}).listen(port);

console.log(`Server is running at port ${port}`);